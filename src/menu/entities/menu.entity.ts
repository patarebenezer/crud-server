import { menu as MenuModel } from '@prisma/client';

export class MenuEntity implements MenuModel {
  id: string;
  name: string;
  price: number;
  stocks: number;
}
