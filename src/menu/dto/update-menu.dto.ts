import { OmitType } from '@nestjs/mapped-types';
import { MenuEntity } from '../entities/menu.entity';

export class UpdateMenuDto extends OmitType(MenuEntity, ['id']) {
  id: string;
  name: string;
  price: number;
  stocks: number;
}
