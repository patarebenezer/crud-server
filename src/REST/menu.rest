###
GET http://localhost:8000/menu

###
GET http://localhost:8000/menu/718602da-1a3d-4653-a9f0-5f1998bf7b50

###
POST http://localhost:8000/menu
Content-Type: application/json

{
 "name": "Mie Aceh",
 "price": 10000,
 "stocks": 5
}

###
PATCH  http://localhost:8000/menu/39d597c6-5e5b-4723-8f54-5b005488c255
Content-Type: application/json

{
 "name": "Iphone 13",
 "price": 9500000.00,
 "stocks": 8
}

###
DELETE  http://localhost:8000/menu/eb3f217c-f288-4d4c-b802-3bf11b1e8177